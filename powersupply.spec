%global srcname powersupply
%global commit 42f6d83b133157b0efb461833ba23011c2243482
%global gittag refs/tags/0.4.0
%global shortcommit %(c=%{commit}; echo ${c:0:7})

Name:           powersupply
Version:        0.4.0
Release:        1%{?dist}
Summary:        GTK3 app to display power status of phones

License:        MIT
URL:            https://gitlab.com/MartijnBraam/%{srcname}
Source0:        https://gitlab.com/MartijnBraam/%{srcname}/-/archive/%{version}/%{srcname}-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  desktop-file-utils
BuildRequires:  python3-gobject gtk3 libhandy


Requires: hicolor-icon-theme
Requires: python3-gobject gtk3 libhandy

%description
GTK3 app to display power status of phones

%prep
%autosetup -n %{srcname}-%{version}

%build
%py3_build

%install
%py3_install
desktop-file-install --dir=%{buildroot}%{_datadir}/applications %{_builddir}/%{srcname}-%{version}/data/nl.brixit.powersupply.desktop
desktop-file-validate %{buildroot}/%{_datadir}/applications/nl.brixit.powersupply.desktop
mkdir -p %{buildroot}/%{_datadir}/icons/hicolor/scalable/apps
cp %{_builddir}/%{srcname}-%{version}/data/nl.brixit.powersupply.svg %{buildroot}/%{_datadir}/icons/hicolor/scalable/apps/nl.brixit.powersupply.svg

%check
%{python3} setup.py test

%files
%defattr(-,root,root,-)
%license LICENSE
%{python3_sitelib}/%{srcname}-*.egg-info/
%{python3_sitelib}/%{srcname}/
%{_bindir}/powersupply
%{_datadir}/applications/nl.brixit.powersupply.desktop
%{_datadir}/icons/hicolor/scalable/apps/nl.brixit.powersupply.svg

%changelog
* Thu Jan 14 2021 congo666 <congo@congo.sk> 0.4.0
- Initial packaging
